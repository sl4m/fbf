use chrono::naive::NaiveDate as Date;
use chrono::Duration;
use colored::*;
use serde::{Deserialize, Serialize};
use serde_yaml::{self};
use std::fs::{self, OpenOptions};
use std::io::BufReader;
use std::path::PathBuf;
// TODO:
//  - Better Colors

static FILE_NAME: &str = "5x5.yaml";

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
enum ExerciseType {
    Squat,
    BenchPress,
    BarbellRow,
    OverHeadPress,
    DeadLift,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
struct Exercise {
    #[serde(rename = "type")]
    ex_type: ExerciseType,
    weight: f64,
    status: Status,
}

impl Exercise {
    fn new(ex_type: ExerciseType, weight: f64, status: Status) -> Self {
        Exercise {
            ex_type,
            weight,
            status,
        }
    }
}
impl Exercise {}
impl fmt::Display for Exercise {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let dash = "-".yellow();
        write!(
            f,
            "{} {:?}: {} Kg *{:?}*",
            dash, self.ex_type, self.weight, self.status
        )
    }
}
#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
pub enum WorkoutType {
    A,
    B,
}

#[derive(Serialize, Deserialize, Debug, PartialEq)]
pub struct Workout {
    #[serde(rename = "type")]
    workout_type: WorkoutType,
    date: Date,
    exercies: [Exercise; 3],
}
use std::fmt;
impl fmt::Display for Workout {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let hash = "#".magenta();
        write!(
            f,
            "{hash} Date: {}\n{hash} Type: {:?}\n{hash} Exercises:\n",
            self.date, self.workout_type
        )?;
        self.exercies.iter().for_each(|e| {
            writeln!(f, "{}", e).unwrap();
        });
        write!(f, "")
    }
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Week {
    num: u64,
    workouts: Vec<Workout>,
}
impl fmt::Display for Week {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        let week_header = format!("Week {}:\n======\n", self.num);
        write!(f, "{}", week_header.cyan()).unwrap();
        self.workouts.iter().for_each(|w| {
            write!(f, "---\n{}\n", w).unwrap();
        });
        write!(f, "")
    }
}

#[derive(Serialize, Deserialize, Debug, PartialEq, Eq)]
enum Status {
    Failed(String),
    Succeeded,
}
pub fn get_log_file() -> Result<PathBuf, std::io::Error> {
    let possible_dirs = &[
        dirs::data_dir().map(|data_dir| data_dir.join(env!("CARGO_CRATE_NAME"))),
        dirs::home_dir(),
        Some(PathBuf::from(".")),
    ];

    //if already exists return it
    for possible_dir in possible_dirs.iter().flatten() {
        let log_file = possible_dir.join(FILE_NAME);
        if log_file.exists() {
            //println!("[!] Using {log_file:?}");
            return Ok(log_file);
        }
    }

    // Otherwise setup the dir/file and return which ever was successfull
    for possible_dir in possible_dirs.iter().flatten() {
        let log_file = possible_dir.join(FILE_NAME);

        // if can't create a dir move to next location
        if fs::create_dir_all(possible_dir).is_err() {
            continue;
        }

        // if can't write to a file within, move to next location
        if OpenOptions::new()
            .create(true)
            .write(true)
            .open(log_file.clone())
            .is_err()
        {
            continue;
        }

        return Ok(log_file);
    }

    // if all 3 possible dirs failed for some weird reason return an Err
    Err(std::io::Error::new(
        std::io::ErrorKind::Other,
        "[!] Failed To Create/Obtain the 5x5 log file in all possible directories!!",
    ))
}

#[derive(Serialize, Deserialize, Debug)]
pub struct Log {
    weeks: Vec<Week>,
}

#[derive(Debug)]
pub enum Error {
    Fs(std::io::Error),
    EditorNotFound,
    InputAborted,
}

impl Log {
    pub fn new() -> Result<Self, Error> {
        let log_file = get_log_file().map_err(Error::Fs)?;
        let reader = BufReader::new(fs::File::open(log_file).map_err(Error::Fs)?);
        match serde_yaml::from_reader(reader) {
            Ok(l) => Ok(l),
            // TODO: Make sure only for io erros and not yaml syntax ones
            Err(_) => Self::init(),
        }
    }
    fn init() -> Result<Self, Error> {
        let input_workout = Self::workout_input(None)?;

        let starter_log = Log {
            weeks: vec![Week {
                num: 1,
                workouts: vec![input_workout],
            }],
        };
        let log_file = get_log_file().unwrap();
        let log_yaml = serde_yaml::to_string(&starter_log).unwrap();
        fs::write(log_file, log_yaml).unwrap();

        Ok(starter_log)
    }

    fn workout_input(base_workout: Option<Workout>) -> Result<Workout, Error> {
        let input_file = "/tmp/fbf_input.txt";

        let dummy_workout_a = Workout {
            date: chrono::Local::now().date_naive(),
            workout_type: WorkoutType::A,
            exercies: [
                Exercise::new(ExerciseType::Squat, 0.0, Status::Succeeded),
                Exercise::new(ExerciseType::BenchPress, 0.0, Status::Succeeded),
                Exercise::new(ExerciseType::BarbellRow, 0.0, Status::Succeeded),
            ],
        };

        let base_workout = base_workout.unwrap_or(dummy_workout_a);

        let workout_yaml = serde_yaml::to_string(&base_workout).unwrap();

        fs::write(input_file, workout_yaml).unwrap();

        let editor = std::env::vars()
            .find(|(var, _)| var == "EDITOR")
            .ok_or(Error::EditorNotFound)?;

        let mut child = std::process::Command::new(editor.1)
            .args([input_file])
            .spawn()
            .expect("failed to execute Editor process");
        child.wait().expect("Failed to wait for editor");

        // read the workout input
        let input = std::fs::read_to_string(input_file).expect("Failed to read input file");
        let input_workout: Workout = serde_yaml::from_str(&input).unwrap(); // fix unwrap

        // Clean input file
        if std::fs::remove_file(input_file).is_err() {
            eprintln!("Could Not delete {input_file}!");
        }

        if input_workout == base_workout {
            return Err(Error::InputAborted);
        }

        Ok(input_workout)
    }

    pub fn get_last_workout(&self) -> Option<&Workout> {
        self.weeks.iter().flat_map(|w| &w.workouts).last()
    }

    pub fn get_week(&self, num: u64) -> Option<&Week> {
        self.weeks.iter().find(|w| w.num == num)
    }

    pub fn get_workout_by_date(&self, date: Date) -> Option<&Workout> {
        self.weeks
            .iter()
            .flat_map(|w| &w.workouts)
            .find(|w| w.date == date)
    }
    pub fn weeks(&self) -> &Vec<Week> {
        &self.weeks
    }

    pub fn add_workout(&mut self) -> Result<(), Error> {
        // add 2.5 kg as default to exercies

        let last_workout = self.get_last_workout().unwrap();

        let input_workout = match last_workout.workout_type {
            WorkoutType::A => {
                let base_workout = Workout {
                    date: last_workout.date + Duration::days(2),
                    workout_type: WorkoutType::B,
                    exercies: [
                        Exercise::new(ExerciseType::Squat, 0.0, Status::Succeeded),
                        Exercise::new(ExerciseType::OverHeadPress, 0.0, Status::Succeeded),
                        Exercise::new(ExerciseType::DeadLift, 0.0, Status::Succeeded),
                    ],
                };

                Self::workout_input(Some(base_workout))
            }
            WorkoutType::B => {
                let base_workout = Workout {
                    date: last_workout.date + Duration::days(2),
                    workout_type: WorkoutType::A,
                    exercies: [
                        Exercise::new(ExerciseType::Squat, 0.0, Status::Succeeded),
                        Exercise::new(ExerciseType::BenchPress, 0.0, Status::Succeeded),
                        Exercise::new(ExerciseType::BarbellRow, 0.0, Status::Succeeded),
                    ],
                };
                Self::workout_input(Some(base_workout))
            }
        }?;

        let latest_week = self.weeks.iter().last().unwrap();
        if latest_week.workouts.len() == 3 {
            let new_week = Week {
                num: latest_week.num + 1,
                workouts: vec![input_workout],
            };
            self.weeks.push(new_week);
        } else {
            self.weeks
                .iter_mut()
                .last()
                .unwrap()
                .workouts
                .push(input_workout);
        }
        self.save();
        Ok(())
    }

    fn save(&self) {
        let log_file = get_log_file().unwrap();
        let log_yaml = serde_yaml::to_string(&self).unwrap();
        fs::write(log_file, log_yaml).unwrap();
    }

    fn edit_workout() {}
    pub fn export(&self) -> Option<String> {
        std::fs::read_to_string(get_log_file().ok()?).ok()
    }
    fn import() {}
}
