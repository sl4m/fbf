use chrono::naive::NaiveDate;
mod log;
use log::Log;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
#[structopt(name = "fbf", about = "CLI util for tacking 5x5 progress")]
struct Opt {
    /// Filter by Week
    #[structopt(short = "w")]
    week: Option<u64>,
    /// Print Workout For a Specific Date
    #[structopt(short = "d")]
    date: Option<NaiveDate>,
    /// Print Last Workout
    #[structopt(short = "l")]
    last: bool,
    /// Add a Workout
    #[structopt(short = "a")]
    new: bool,
    /// Export The YAML log file
    #[structopt(short = "e")]
    export: bool,
}

fn main() {
    match Log::new() {
        Ok(mut log) => {
            // Handle CLI Args
            let opt = Opt::from_args();

            if let Some(week) = opt.week {
                match log.get_week(week) {
                    Some(week) => {
                        print!("{}", week);
                    }
                    None => {
                        eprintln!("Not Found");
                    }
                }
            } else if let Some(date) = opt.date {
                match log.get_workout_by_date(date) {
                    Some(workout) => {
                        print!("{}", workout);
                    }
                    None => {
                        eprintln!("Not Found");
                    }
                }
            } else if opt.export {
                print!("{}", log.export().unwrap_or_else(|| "Failed!".into()));
            } else if opt.last {
                println!("{}", log.get_last_workout().unwrap());
            } else if opt.new {
                if log.add_workout().is_err() {
                    eprintln!("[!] Operation Aborted!");
                }
            } else {
                log.weeks().iter().for_each(|w| println!("{}", w));
            }
        }
        Err(e) => match e {
            log::Error::EditorNotFound => {
                eprintln!("could not obtain the default text editor through the EDITOR environment variable");
            }
            _ => {
                eprintln!("{e:?}");
            }
        },
    }
}
